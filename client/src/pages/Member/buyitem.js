import React from 'react';
import './buyitem.css';
import qrcode from './Image/qrcode.jpg';
import Navbar from '../../component/navbar/MemberNavbar';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

const BuyItemPage = () => {
    const navigate = useNavigate(); 

    const handleUploadSlip = () => {
        Swal.fire({
            title: 'อัพโหลดสลิป',
            text: 'กรุณาอัพโหลดสลิปการโอนเงิน',
            icon: 'info',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก',
            input: 'file',
            inputAttributes: {
                accept: '.pdf, .jpg, .jpeg, .png',
                'aria-label': 'Upload your slip',
            }
        }).then((file) => {
            if (file.value) {
                const uploadedFile = file.value;
              
                console.log('Uploaded file:', uploadedFile);
                Swal.fire({
                    title: 'การสั่งซื้อสำเร็จ!',
                    text: 'สลิปของคุณถูกอัพโหลดเรียบร้อยแล้ว โปรดรอแอดมินทำการตรวจสอบ',
                    icon: 'success',
                }).then(() => {
                    navigate('/member/profile/waitpay');
                });
            }
        });
    };

    return (
        <div className='bg'>
            <Navbar /><br></br><br></br>
            <div className="order-confirmation-container text-center">
                <p className="title">คุณได้ทำการสั่งซื้อแล้ว!</p>
                <p className="message">
                </p>
                <p class="black-text">กรุณาสแกน QrCode เพื่อชำระเงินและเพื่อความสะดวกโปรดทำการติดต่อแอดมินเพื่อส่งสลิปการโอนเงินผ่านช่องทางการติดต่อต่างๆที่ได้ให้ไว้</p>
                <img
                    src={qrcode}
                    alt="QR Code"
                    className="qr-code"
                /><br></br>
                <button className="btn btn-primary" onClick={handleUploadSlip}>อัพโหลดสลิป</button>
            </div>
        </div>
    );
};

export default BuyItemPage;
