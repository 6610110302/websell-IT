import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from 'react-bootstrap/Card';
import Navbar from '../../component/navbar/MemberNavbar';
import Carousel from 'react-bootstrap/Carousel';
import ProductModal from './ProductModal';
import useLocalState from '../../component/utils/useLocalStorage'; // ตรวจสอบเส้นทางนี้ให้ถูกต้อง
import { useNavigate } from 'react-router-dom';

const Home = () => {
    const [products, setProducts] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [modalShow, setModalShow] = useState(false);
    const [loading, setLoading] = useState(true);
    const [auth] = useLocalState({}, 'auth');
    const navigate = useNavigate();
    console.log(auth.jwt); 

    const handleShowModal = (product) => {
        setSelectedProduct(product);
        setModalShow(true);
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const config = auth && auth.jwt ? { 
                    headers: { Authorization: `Bearer ${auth.jwt}` }
                } : {};
  
                const response = await axios.get('http://localhost:1337/api/products?filters[status][$eq]=สินค้าแนะนำประจำวัน', config);
                setProducts(response.data.data);
            } catch (error) {
                console.error('Error fetching data:', error);
                navigate('/login');
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [auth, navigate]);
    
    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <div className="main">
            <Navbar />
            <br></br>
            <div>
                <Carousel>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="https://i.postimg.cc/HnJMwkxN/4488646846848.png"
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="https://i.postimg.cc/cHWZ79JG/2.png"
                            alt="Second slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="https://i.postimg.cc/15wkB0L9/3.png"
                            alt="Third slide"
                        />
                    </Carousel.Item>
                </Carousel>
            </div><br></br><br></br>
            <div className="container margin1"><br></br>
                <h3 className='text-left' style={{ color: '#003AA7' }}>สินค้าแนะนำประจำวัน</h3>
                <div className="row">
                    {products.map((product) => (
                        <div className="col-md-3 my-3 homepage-card" key={product.id} onClick={() => handleShowModal(product)}> 
                            <Card style={{ width: '18rem' }}>
                                <Card.Img variant="top" src={product.attributes.linkimage} style={{ objectFit: 'cover', height: '15rem' }} />
                                <Card.Body>
                                    <Card.Title>{product.attributes.name}</Card.Title>
                                    <Card.Text>
                                        <h4 style={{ color: '#003AA7' }}>฿{product.attributes.price}</h4>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                            </div>
                    ))}
                </div>
            </div>
            {selectedProduct && (
                <ProductModal
                    show={modalShow}
                    onHide={() => setModalShow(false)}
                    product={selectedProduct}
                />
            )}
        </div>
    );
};


export default Home;
